package com.whoisthatgoodboi.Base

import android.content.Context
import android.hardware.Camera
import com.whoisthatgoodboi.Extentions.*

class Config(context: Context) {
    companion object {
        fun newInstance(context: Context) = Config(context)
    }

    protected val prefs = context.getSharedPrefs()

    var focusBeforeCapture: Boolean
        get() = prefs.getBoolean(FOCUS_BEFORE_CAPTURE, true)
        set(focus) = prefs.edit().putBoolean(FOCUS_BEFORE_CAPTURE, focus).apply()

    var isShowPreviewEnabled: Boolean
        get() = prefs.getBoolean(SHOW_PREVIEW, false)
        set(enabled) = prefs.edit().putBoolean(SHOW_PREVIEW, enabled).apply()

    var wasPhotoPreviewHintShown: Boolean
        get() = prefs.getBoolean(PHOTO_PREVIEW_HINT_SHOWN, false)
        set(wasPhotoPreviewHintShown) = prefs.edit().putBoolean(PHOTO_PREVIEW_HINT_SHOWN, wasPhotoPreviewHintShown).apply()

    var backPhotoResIndex: Int
        get() = prefs.getInt(BACK_PHOTO_RESOLUTION_INDEX, -1)
        set(backPhotoResIndex) = prefs.edit().putInt(BACK_PHOTO_RESOLUTION_INDEX, backPhotoResIndex).apply()

    var alwaysOpenBackCamera: Boolean
        get() = prefs.getBoolean(ALWAYS_OPEN_BACK_CAMERA, false)
        set(alwaysOpenBackCamera) = prefs.edit().putBoolean(ALWAYS_OPEN_BACK_CAMERA, alwaysOpenBackCamera).apply()

    var lastUsedCamera: Int
        get() = prefs.getInt(LAST_USED_CAMERA, Camera.CameraInfo.CAMERA_FACING_BACK)
        set(cameraId) = prefs.edit().putInt(LAST_USED_CAMERA, cameraId).apply()

    var turnFlashOffAtStartup: Boolean
        get() = prefs.getBoolean(TURN_FLASH_OFF_AT_STARTUP, false)
        set(turnFlashOffAtStartup) = prefs.edit().putBoolean(TURN_FLASH_OFF_AT_STARTUP, turnFlashOffAtStartup).apply()

    var flashlightState: Int
        get() = prefs.getInt(FLASHLIGHT_STATE, FLASH_OFF)
        set(state) = prefs.edit().putInt(FLASHLIGHT_STATE, state).apply()

    var keepSettingsVisible: Boolean
        get() = prefs.getBoolean(KEEP_SETTINGS_VISIBLE, false)
        set(keepSettingsVisible) = prefs.edit().putBoolean(KEEP_SETTINGS_VISIBLE, keepSettingsVisible).apply()
}