package com.whoisthatgoodboi.Camera

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.hardware.Camera
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.Toast
import com.whoisthatgoodboi.Base.Config
import com.whoisthatgoodboi.Extentions.*
import com.whoisthatgoodboi.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Preview : ViewGroup, SurfaceHolder.Callback, MediaScannerConnection.OnScanCompletedListener {

    var mCamera : Camera? = null
    private val FOCUS_AREA_SIZE = 100
    private val PHOTO_PREVIEW_LENGTH = 5000L
    private val REFOCUS_PERIOD = 3000L

    lateinit var mSurfaceHolder: SurfaceHolder
    lateinit var mSurfaceView: SurfaceView
    lateinit var mCallback : PreviewListener
    lateinit var mScreenSize : Point
    lateinit var config : Config

    private var mSupportedPreviewSizes : List<Camera.Size>? = null
    private var mPreviewSize : Camera.Size? = null
    private var mParameters : Camera.Parameters? = null
    private var mScaleGestureDetector : ScaleGestureDetector? = null
    private var mZoomRatios = ArrayList<Int>()

    private var mCanTakePicture = false
    private var mIsSurfaceCreated = false
    private var mSetupPreviewAfterMeasure = false
    private var mIsSixteenToNine = false
    private var mWasZooming = false
    private var mIsPreviewShown = false
    private var mWasCameraPreviewSet = false
    private var mLastClickX = 0
    private var mLastClickY = 0
    private var mCurrentCameraId = 0
    private var mMaxZoom = 0
    private var mRotationAtCapture = 0
    private var mIsFocusingBeforeCapture = false
    private var mAutoFocusHandler = Handler()

    var mActivity : CameraActivityMain? = null
    var mIsWaitingForTakePictureCallback  = false
    var mTargetUri : Uri? = null
    var mIsImageCaptureIntent = false

    constructor(context: Context) : super(context)

    constructor(activity : CameraActivityMain, surfaceView: SurfaceView, previewListener: PreviewListener) : super(activity) {
        mActivity = activity
        mCallback = previewListener
        mSurfaceView = surfaceView
        mSurfaceHolder = mSurfaceView.holder
        mSurfaceHolder.addCallback(this)
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        mCanTakePicture = false
        mIsSurfaceCreated = false
        mSetupPreviewAfterMeasure = false
        config = activity.config
        mScreenSize = getScreenSize()
        initGestureDetector()

        setSurfaceViewListener(false)

        mSurfaceView.setOnClickListener {
            if (mIsPreviewShown) {
                resumePreview()
            } else {
                if (!mWasZooming && !mIsPreviewShown) {
                    focusArea(false)
                }

                mWasZooming = false
                mSurfaceView.isSoundEffectsEnabled = true
            }
        }
    }

    private fun setSurfaceViewListener(ifEmpty : Boolean) {
        if (ifEmpty) {
            mSurfaceView.setOnClickListener {  }
        } else {
            mSurfaceView.setOnTouchListener {
                view, event ->
                mLastClickX = event.x.toInt()
                mLastClickY = event.y.toInt()

                if (mMaxZoom > 0 && mParameters?.isZoomSupported == true) {
                    mScaleGestureDetector!!.onTouchEvent(event)
                }
                false
            }
        }
    }

    private fun getScreenSize() : Point {
        val display = mActivity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    private fun initGestureDetector() {
        mScaleGestureDetector = ScaleGestureDetector(mActivity, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector?): Boolean {
                val zoomFactor = mParameters!!.zoom
                var zoomRatio = mZoomRatios[zoomFactor] / 100f
                zoomRatio *= detector!!.scaleFactor

                var newZoomFactor = zoomFactor
                if (zoomRatio <= 1f) {
                    newZoomFactor = 0
                } else if (zoomRatio >= (mZoomRatios[mMaxZoom] / 100f)) {
                    newZoomFactor = mMaxZoom
                } else {
                    if (detector.scaleFactor > 1f) {
                        for (i in zoomFactor until mZoomRatios.size) {
                            if (mZoomRatios[i] / 100.0f >= zoomRatio) {
                                newZoomFactor = i
                                break
                            }
                        }
                    } else {
                        for (i in zoomFactor downTo 0) {
                            if (mZoomRatios[i] / 100.0f <= zoomRatio) {
                                newZoomFactor = i
                                break
                            }
                        }
                    }
                }

                newZoomFactor = Math.max(newZoomFactor, 0)
                newZoomFactor = Math.min(mMaxZoom, newZoomFactor)

                mParameters!!.zoom = newZoomFactor
                updateCameraParameters()
                return true
            }

            override fun onScaleEnd(detector: ScaleGestureDetector?) {
                super.onScaleEnd(detector)
                mWasZooming = true
                mSurfaceView.isSoundEffectsEnabled = false
                mParameters!!.focusAreas = null
            }
        })
    }

    private fun resumePreview() {
        mActivity!!.setClassificationButtonVisibility(View.GONE)
        mIsPreviewShown = false
        try {
            mCamera?.startPreview()
        } catch (ignored : Exception) {
            //ignored
        }
        mCanTakePicture = true
        focusArea(false, false)
    }

    private fun focusArea(takePictureAfter : Boolean, showFocusRect : Boolean = true) {
        if (mCamera == null || (mIsFocusingBeforeCapture && !takePictureAfter)) {
            return
        }

        if (takePictureAfter) {
            mIsFocusingBeforeCapture = true
        }

        mCamera!!.cancelAutoFocus()
        if (mParameters!!.maxNumFocusAreas > 0) {
            if (mLastClickX == 0 && mLastClickY == 0) {
                mLastClickX = width / 2
                mLastClickY = height / 2
            }

            val focusRect = calculateFocusArea(mLastClickX.toFloat(), mLastClickY.toFloat())
            val focusAreas = ArrayList<Camera.Area>(1)
            focusAreas.add(Camera.Area(focusRect, 1000))
            mParameters!!.focusAreas = focusAreas

            if (showFocusRect) {
                mCallback.drawFocusRect(mLastClickX, mLastClickY)
            }
        }

        try {
            val focusModes = mParameters!!.supportedFocusModes
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                mParameters!!.focusMode = Camera.Parameters.FOCUS_MODE_AUTO
            }

            updateCameraParameters()
            mCamera!!.autoFocus {
                success, camera ->
                    if (camera == null || mCamera == null) {
                        return@autoFocus
                    }

                    mCamera!!.cancelAutoFocus()
                    if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        mParameters!!.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
                    }

                    updateCameraParameters()

                    if (takePictureAfter) {
                        takePicture()
                    } else {
                        rescheduleAutoFocus()
                    }
            }
        } catch (ignored : Exception) {
            //ignored
        }
    }

    private fun updateCameraParameters() {
        try {
            mCamera?.parameters = mParameters
        } catch (e : RuntimeException) {
            //exception handle
        }
    }

    private fun calculateFocusArea(x : Float, y: Float) : Rect {
        var left = x / mSurfaceView.width * 2000 - 1000
        var top = y / mSurfaceView.height * 2000 - 1000

        val tmp = left
        left = top
        top = -tmp

        val rectLeft = Math.max(left.toInt() - FOCUS_AREA_SIZE / 2, -1000)
        val rectTop = Math.max(top.toInt() - FOCUS_AREA_SIZE / 2, -1000)
        val rectRight = Math.min(left.toInt() + FOCUS_AREA_SIZE / 2, 1000)
        val rectBottom = Math.min(top.toInt() + FOCUS_AREA_SIZE / 2, 1000)
        return Rect(rectLeft, rectTop, rectRight, rectBottom)
    }

    fun tryTakePicture() {
        if (config.focusBeforeCapture) {
            focusArea(true)
        } else {
            takePicture()
        }
    }

    @SuppressLint("ShowToast")
    private fun takePicture() {
        if (mCanTakePicture) {
            val selectedResolution = getSelectedResolution()
            mParameters!!.setPictureSize(selectedResolution.width, selectedResolution.height)
            val pictureSize = mParameters!!.pictureSize
            if (selectedResolution.width != pictureSize.width || selectedResolution.height != pictureSize.height) {
                Toast.makeText(mActivity, R.string.setting_resolution_error, Toast.LENGTH_SHORT)
            }

            mRotationAtCapture = mActivity!!.mLastHandledOrientation
            updateCameraParameters()
            mIsWaitingForTakePictureCallback = true
            mIsPreviewShown = true

            try {
                Thread {
                    mCamera!!.takePicture(null, null, takePictureCallback)
                }.start()
            } catch (ignored : Exception) {
                //ignored
            }

            mCanTakePicture = false
            mIsFocusingBeforeCapture = false
        }
    }

    @SuppressLint("ShowToast")
    private val takePictureCallback = Camera.PictureCallback {
        data, cam ->
            if (data.isEmpty()) {
                Toast.makeText(mActivity, R.string.unknown_error, Toast.LENGTH_SHORT)
                return@PictureCallback
            }

        mIsWaitingForTakePictureCallback = false
        if (!mIsImageCaptureIntent) {
            handlePreview()
        }

        if (mIsImageCaptureIntent) {
            if (mTargetUri != null) {
                mActivity!!.mPhoto = storePhoto(data)
            } else {
                mActivity!!.finishActivity()
            }
        } else {
            mActivity!!.mPhoto = storePhoto(data)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun storePhoto(data : ByteArray): String {
        val photo : Bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
        Log.e("BitmapSize", "width: " + photo.width + " height: " + photo.height)
        Log.e("PhotoOrientation", "orientation: " + mActivity!!.mLastHandledOrientation)
        val matrix = Matrix()
        if (mActivity!!.mLastHandledOrientation == 0) {
            matrix.postRotate(90F)
        } else {
            matrix.postRotate(0F)
        }
        val rotatedPhoto = Bitmap.createBitmap(photo, 0, 0, photo.width, photo.height, matrix, true)

        //val photoPath = tempFileSave(rotatedPhoto, "name")
        val sdf = SimpleDateFormat("yyyy-mm-dd_hh-mm-ss")
        val currDate = sdf.format(Date())

        val photoPath = savePhotoToGallery(rotatedPhoto, "doggo" + currDate)

        return photoPath
    }

    private fun savePhotoToGallery(bitmap: Bitmap, name: String) : String {
        val savedImageUrl = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, name, "Image of $name")
        return savedImageUrl
    }

    private fun classificationOn() {
        mActivity!!.setClassificationButtonVisibility(View.VISIBLE)
    }

    @SuppressLint("ShowToast")
    private fun handlePreview() {
        setSurfaceViewListener(true)
        if (config.isShowPreviewEnabled) {
            if (!config.wasPhotoPreviewHintShown) {
                Toast.makeText(mActivity, R.string.resume_preview_error, Toast.LENGTH_SHORT)
                config.wasPhotoPreviewHintShown = true
            }
        } else {
            classificationOn()
        }
    }

    private fun rescheduleAutoFocus() {
        mAutoFocusHandler.removeCallbacksAndMessages(null)
        mAutoFocusHandler.postDelayed({}, REFOCUS_PERIOD)
    }

    private fun getSelectedResolution() : Camera.Size {
        if (mParameters == null) {
            mParameters = mCamera!!.parameters
        }

        val resolutions = mParameters!!.supportedPictureSizes.sortedByDescending { it.width * it.height }
        val index = getDefaultFullScreenResolution(resolutions) ?: 0
        return resolutions[index]
    }

    private fun getDefaultFullScreenResolution(resolutions : List<Camera.Size>) : Int? {
        val screenAspectRatio = mActivity!!.realScreenSize.y / mActivity!!.realScreenSize.x.toFloat()
        resolutions.forEachIndexed {
            index, size ->
                val diff = screenAspectRatio - (size.width / size.height.toFloat())
                if (Math.abs(diff) < RATIO_TOLERANCE) {
                    config.backPhotoResIndex = index
                    return index
                }
        }
        return null
    }

    @SuppressLint("ShowToast")
    fun setCamera(cameraId : Int) : Boolean {
        mCurrentCameraId = cameraId
        val newCamera : Camera
        try {
            newCamera = Camera.open(cameraId)
            mCallback.setIsCameraAvailable(true)
        } catch (e : Exception) {
            Toast.makeText(mActivity, e.message, Toast.LENGTH_SHORT)
            mCallback.setIsCameraAvailable(false)
            return false
        }

        if (mCamera == newCamera) {
            return false
        }

        releaseCamera()
        mCamera = newCamera
        initCamera()
        if (!mWasCameraPreviewSet && mIsSurfaceCreated) {
            mCamera!!.setPreviewDisplay(mSurfaceHolder)
            mWasCameraPreviewSet = true
        }
        return true
    }

    @SuppressLint("ShowToast")
    private fun initCamera() : Boolean {
        if (mCamera == null) {
            return false
        }

        mParameters = mCamera!!.parameters
        mMaxZoom = mParameters!!.maxZoom

        if (mParameters!!.isZoomSupported) {
            mZoomRatios = mParameters!!.zoomRatios as ArrayList<Int>
        }

        mSupportedPreviewSizes = mParameters!!.supportedPreviewSizes.sortedByDescending { it.width * it.height }
        refreshPreview()

        val focusModes = mParameters!!.supportedFocusModes
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            mParameters!!.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
        }

        mCamera!!.setDisplayOrientation(mActivity!!.getPreviewRotation(mCurrentCameraId))
        updateCameraParameters()

        if (mCanTakePicture) {
            try {
                mCamera!!.setPreviewDisplay(mSurfaceHolder)
            } catch (e : IOException) {
                Toast.makeText(mActivity, e.message, Toast.LENGTH_SHORT)
                return false
            }
        }

        mCallback.setFlashAvailable(hasFlash(mCamera))
        return true
    }

    private fun refreshPreview() {
        mIsSixteenToNine = getSelectedResolution().isSixteenToNine()
        mSetupPreviewAfterMeasure = true
        requestLayout()
        invalidate()
        rescheduleAutoFocus()
    }

    fun releaseCamera() {
        mCamera?.stopPreview()
        mCamera?.release()
        mCamera = null
    }

    override fun onLayout(p0: Boolean, p1: Int, p2: Int, p3: Int, p4: Int) {
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        mIsSurfaceCreated = true
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        mIsSurfaceCreated = false
        mCamera?.stopPreview()
    }

    @SuppressLint("ShowToast")
    override fun surfaceCreated(holder: SurfaceHolder?) {
        mIsSurfaceCreated = true
        try {
            mWasCameraPreviewSet = mCamera != null
            mCamera?.setPreviewDisplay(mSurfaceHolder)
        } catch (e : IOException) {
            Toast.makeText(mActivity, e.message, Toast.LENGTH_SHORT)
        }
    }

    override fun onScanCompleted(p0: String?, p1: Uri?) {
    }

    @SuppressLint("ShowToast")
    private fun setupPreview() {
        mCanTakePicture = true
        if (mCamera != null && mPreviewSize != null) {
            if (mParameters == null) {
                mParameters = mCamera!!.parameters
            }

            mParameters!!.setPreviewSize(mPreviewSize!!.width, mPreviewSize!!.height)
            updateCameraParameters()
            try {
                mCamera!!.startPreview()
            } catch (e : RuntimeException) {
                Toast.makeText(mActivity, e.message, Toast.LENGTH_SHORT)
            }
        }
    }

    private fun getOptimalPreviewSize(sizes: List<Camera.Size>, width: Int, height: Int): Camera.Size {
        var result: Camera.Size? = null
        for (size in sizes) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size
                } else {
                    val resultArea = result.width * result.height
                    val newArea = size.width * size.height

                    if (newArea > resultArea) {
                        result = size
                    }
                }
            }
        }

        return result!!
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(mScreenSize.x, mScreenSize.y)

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = if (mIsSixteenToNine) {
                getOptimalPreviewSize(mSupportedPreviewSizes!!, mScreenSize.y, mScreenSize.x)
            } else {
                val newRatioHeight = (mScreenSize.x * (4.toDouble() / 3)).toInt()
                setMeasuredDimension(mScreenSize.x, newRatioHeight)
                getOptimalPreviewSize(mSupportedPreviewSizes!!, newRatioHeight, mScreenSize.x)
            }
            val lp = mSurfaceView.layoutParams

            if (mScreenSize.x > mPreviewSize!!.height) {
                val ratio = mScreenSize.x.toFloat() / mPreviewSize!!.height
                lp.width = (mPreviewSize!!.height * ratio).toInt()
                if (mIsSixteenToNine) {
                    lp.height = mScreenSize.y
                } else {
                    lp.height = (mPreviewSize!!.width * ratio).toInt()
                }
            } else {
                lp.width = mPreviewSize!!.height
                lp.height = mPreviewSize!!.width
            }

            if (mSetupPreviewAfterMeasure) {
                if (mCamera != null) {
                    mSetupPreviewAfterMeasure = false
                    mCamera!!.stopPreview()
                    setupPreview()
                }
            }
        }
    }

    fun enableFlash() {
        mParameters!!.flashMode = Camera.Parameters.FLASH_MODE_TORCH
        updateCameraParameters()
    }

    fun disableFlash() {
        mParameters!!.flashMode = Camera.Parameters.FLASH_MODE_OFF
        updateCameraParameters()
    }

    fun autoFlash() {
        mParameters!!.flashMode = Camera.Parameters.FLASH_MODE_OFF
        updateCameraParameters()

        Handler().postDelayed({
            mActivity?.runOnUiThread {
                mParameters?.flashMode = Camera.Parameters.FLASH_MODE_AUTO
            }
        }, 1000)
    }

    private fun hasFlash(camera: Camera?): Boolean {
        if (camera == null) {
            return false
        }

        if (camera.parameters.flashMode == null) {
            return false
        }

        val supportedFlashModes = camera.parameters.supportedFlashModes
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() ||
                supportedFlashModes.size == 1 && supportedFlashModes[0] == Camera.Parameters.FLASH_MODE_OFF) {
            return false
        }

        return true
    }

    interface PreviewListener {
        fun setFlashAvailable(available: Boolean)

        fun setIsCameraAvailable(available: Boolean)

        fun videoSaved(uri: Uri)

        fun drawFocusRect(x: Int, y: Int)
    }
}