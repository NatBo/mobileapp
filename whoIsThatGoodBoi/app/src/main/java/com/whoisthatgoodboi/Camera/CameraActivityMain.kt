package com.whoisthatgoodboi.Camera

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.hardware.Camera
import android.hardware.SensorManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.whoisthatgoodboi.Base.BaseSimpleActivity
import com.whoisthatgoodboi.Classification.ClassificationActivity
import com.whoisthatgoodboi.Extentions.*
import com.whoisthatgoodboi.R
import com.whoisthatgoodboi.R.id.classification
import kotlinx.android.synthetic.main.activity_camera_main.*

//TODO: cleaning here
//TODO: classification button not working properly -> showing too late (find why)
//TODO: here or in Preview: after take photo show button to classify or discard -> if classify save photo and then classify
//TODO: find dogs faces and classify just faces


class CameraActivityMain : BaseSimpleActivity(), Preview.PreviewListener {

    private val FADE_DELAY = 5000
    private val IMAGE_REQUEST_CODE = 10

    lateinit var mFadeHandler: Handler
    private var mPreview: Preview? = null
    private var mFlashlightState = FLASH_OFF
    private var mIsCameraAvailable = false
    private var mIsHardwareShutterHandled = false
    private var mCurrCameraId = 0
    var mLastHandledOrientation = 0
    lateinit var mPhoto: String

    lateinit var mOrientationEventListener: OrientationEventListener

    override fun onCreate(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (config.alwaysOpenBackCamera) {
            config.lastUsedCamera = Camera.CameraInfo.CAMERA_FACING_BACK
        }

//        setContentView(R.layout.activity_camera_main)

        initVariables()
        tryInitCamera()
//        initClassificationListener()
        setupOrientationEventListener()
    }

    override fun onResume() {
        super.onResume()
        if (hasStorageAndCameraPermissions()) {
            resumeCameraItems()
        }

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (hasStorageAndCameraPermissions()) {
            mOrientationEventListener.enable()
        }
    }

    @SuppressLint("ShowToast")
    override fun onPause() {
        super.onPause()
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (!hasStorageAndCameraPermissions() || isAskingPermissions) {
            return
        }

        mFadeHandler.removeCallbacksAndMessages(null)

        mPreview?.releaseCamera()
        mOrientationEventListener.disable()

        if (mPreview?.mIsWaitingForTakePictureCallback == true) {
            Toast.makeText(this, R.string.photo_not_saved, Toast.LENGTH_SHORT)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPreview?.releaseCamera()
        mPreview?.mActivity = null
        mPreview = null
    }

    private fun initVariables() {
        mIsCameraAvailable = false
        mIsHardwareShutterHandled = false
        mCurrCameraId = 0
        mLastHandledOrientation = 0
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_CAMERA && !mIsHardwareShutterHandled) {
            mIsHardwareShutterHandled = true
            shutterPressed()
            true
        } else {
            super.onKeyDown(keyCode, event)
        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_CAMERA) {
            mIsHardwareShutterHandled = false
        }
        return super.onKeyUp(keyCode, event)
    }

    private fun initClassificationListener() {
        classification.setOnClickListener({
            goToClassificationActivity()
        })
    }

    @SuppressLint("ShowToast")
    private fun tryInitCamera() {
        handlePermission(PERMISSION_CAMERA) {
            if (it) {
                handlePermission(PERMISSION_WRITE_STORAGE) {
                    if (it) {
                        initializeCamera()
                        handleIntent()
                    } else {
                        Toast.makeText(this, R.string.no_storage_permission_error, Toast.LENGTH_SHORT)
                        finish()
                    }
                }
            } else {
                Toast.makeText(this, R.string.no_camera_permission_error, Toast.LENGTH_SHORT)
                finish()
            }
        }
    }

    private fun handleIntent() {
        if (isImageCaptureIntent()) {
            val output = intent.extras?.get(MediaStore.EXTRA_OUTPUT)
            if (output != null && output is Uri) {
                mPreview?.mTargetUri = output
            }
        }
        mPreview?.mIsImageCaptureIntent = isImageCaptureIntent()
    }

    private fun isImageCaptureIntent() = intent?.action == MediaStore.ACTION_IMAGE_CAPTURE || intent?.action == MediaStore.ACTION_IMAGE_CAPTURE_SECURE

    private fun initializeCamera() {
        setContentView(R.layout.activity_camera_main)
        initButtons()
        initClassificationListener()

        (btn_holder.layoutParams as RelativeLayout.LayoutParams)
                .setMargins(0, 0, 0,
                        (resources.getDimension(R.dimen.regular)).toInt())

        mCurrCameraId = config.lastUsedCamera
        mPreview = Preview(this, camera_view, this)
        mPreview!!.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        view_holder.addView(mPreview)

        mFadeHandler = Handler()
        mFlashlightState = if (config.turnFlashOffAtStartup) FLASH_OFF else config.flashlightState
    }

    private fun initButtons() {
        toggle_camera.setOnClickListener { toggleCamera() }
        last_photo_video_preview.setOnClickListener { showLastMediaPreview() }
        toggle_flash.setOnClickListener { toggleFlash() }
        shutter.setOnClickListener { shutterPressed() }
    }

    @SuppressLint("ShowToast")
    private fun toggleCamera() {
        if (!checkCameraAvailable()) {
            return
        }

        mCurrCameraId = if (mCurrCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            Camera.CameraInfo.CAMERA_FACING_FRONT
        } else {
            Camera.CameraInfo.CAMERA_FACING_BACK
        }

        config.lastUsedCamera = mCurrCameraId
        mPreview?.releaseCamera()
        if (mPreview?.setCamera(mCurrCameraId) == true) {
            disableFlash()
        } else {
            Toast.makeText(this, R.string.camera_switch_error, Toast.LENGTH_SHORT)
        }
    }

    private fun showLastMediaPreview() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, IMAGE_REQUEST_CODE)
    }

    private fun toggleFlash() {
        if (!checkCameraAvailable()) {
            return
        }

        mFlashlightState = ++mFlashlightState % 3
        checkFlash()
    }

    private fun checkFlash() {
        when (mFlashlightState) {
            FLASH_ON -> enableFlash()
            FLASH_AUTO -> autoFlash()
            else -> disableFlash()
        }
    }

    private fun disableFlash() {
        mPreview?.disableFlash()
        toggle_flash.setImageResource(R.drawable.ic_flash_off)
        mFlashlightState = FLASH_OFF
        config.flashlightState = FLASH_OFF
    }

    private fun enableFlash() {
        mPreview?.enableFlash()
        toggle_flash.setImageResource(R.drawable.ic_flash_on)
        mFlashlightState = FLASH_ON
        config.flashlightState = FLASH_ON
    }

    private fun autoFlash() {
        mPreview?.autoFlash()
        toggle_flash.setImageResource(R.drawable.ic_flash_on)
        mFlashlightState = FLASH_AUTO
        config.flashlightState = FLASH_AUTO
    }

    private fun shutterPressed() {
        if (checkCameraAvailable()) {
            handleShutter()
        }
    }

    private fun handleShutter() {
        mPreview?.tryTakePicture()
    }

    private fun fadeOutButtons() {
        fadeAnim(settings, .5f)
        fadeAnim(last_photo_video_preview, .0f)
    }

    private fun fadeAnim(view: View, value: Float) {
        view.animate().alpha(value).start()
        view.isClickable = value != .0f
    }

    private fun hasStorageAndCameraPermissions() = hasPermission(PERMISSION_WRITE_STORAGE) && hasPermission(PERMISSION_CAMERA)

    private fun setupOrientationEventListener() {
        mOrientationEventListener = object : OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @SuppressLint("ObsoleteSdkInt")
            override fun onOrientationChanged(orientation: Int) {
                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) && isDestroyed) {
                    mOrientationEventListener.disable()
                    return
                }

                val currOrient = when (orientation) {
                    in 75..134 -> ORIENT_LANDSCAPE_RIGHT
                    in 225..289 -> ORIENT_LANDSCAPE_LEFT
                    else -> ORIENT_PORTRAIT
                }

                if (currOrient != mLastHandledOrientation) {
                    val degrees = when (currOrient) {
                        ORIENT_LANDSCAPE_LEFT -> 90
                        ORIENT_LANDSCAPE_RIGHT -> -90
                        else -> 0
                    }

                    animateViews(degrees)
                    mLastHandledOrientation = currOrient
                }
            }
        }
    }

    private fun animateViews(degrees: Int) {
        val views = arrayOf<View>(toggle_camera, toggle_flash, shutter, settings, last_photo_video_preview)
        for (view in views) {
            rotate(view, degrees)
        }
    }

    private fun rotate(view: View, degrees: Int) = view.animate().rotation(degrees.toFloat()).start()

    @SuppressLint("ShowToast")
    private fun checkCameraAvailable(): Boolean {
        if (!mIsCameraAvailable) {
            Toast.makeText(this, R.string.camera_not_available_error, Toast.LENGTH_SHORT)
        }
        return mIsCameraAvailable
    }

    fun finishActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    fun setClassificationButtonVisibility(visibility : Int) {
        classification.visibility = visibility
    }

    override fun setFlashAvailable(available: Boolean) {
        if (available) {
            toggle_flash.visibility = View.VISIBLE
        } else {
            toggle_flash.visibility = View.INVISIBLE
            disableFlash()
        }
    }

    override fun setIsCameraAvailable(available: Boolean) {
        mIsCameraAvailable = available
    }

    override fun drawFocusRect(x: Int, y: Int) {} // = mFocusRectView.drawFocusRect(x, y)

    override fun videoSaved(uri: Uri) {}

    @SuppressLint("ShowToast")
    private fun resumeCameraItems() {
        showToggleCameraIfNeeded()
        if (mPreview?.setCamera(mCurrCameraId) == true) {
            checkFlash()

        } else {
            Toast.makeText(this, R.string.camera_switch_error, Toast.LENGTH_SHORT)
        }
    }

    private fun showToggleCameraIfNeeded() {
        if (Camera.getNumberOfCameras() <= 1) {
            toggle_camera?.visibility = View.INVISIBLE
        } else {
            toggle_camera?.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when {
                requestCode == IMAGE_REQUEST_CODE -> {
                    manageImageFromUri(data!!.data)
                }
            }
        } else {
            Toast.makeText(this, "No image", Toast.LENGTH_SHORT)
        }
    }

    private fun manageImageFromUri(imageUri: Uri) {
        var bitmap : Bitmap? = null

        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        } catch (ex : Exception) {

        }

        if (bitmap != null) {
            mPhoto = imageUri.toString()
            goToClassificationActivity()
        }
    }

    private fun goToClassificationActivity() {
        val intent = Intent(this, ClassificationActivity::class.java)
        intent.putExtra("photo", mPhoto)
        startActivity(intent)
    }
}