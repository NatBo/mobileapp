package com.whoisthatgoodboi

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.whoisthatgoodboi.Camera.CameraActivityMain

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, CameraActivityMain::class.java)
        startActivity(intent)
    }
}
