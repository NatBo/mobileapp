package com.whoisthatgoodboi.Extentions

import android.database.Cursor

fun Cursor.getLongValue(key: String) = getLong(getColumnIndex(key))