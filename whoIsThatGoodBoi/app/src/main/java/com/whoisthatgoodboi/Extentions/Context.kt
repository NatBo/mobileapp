package com.whoisthatgoodboi.Extentions

import android.content.Context
import android.database.Cursor
import android.graphics.Point
import android.net.Uri
import android.provider.BaseColumns
import android.provider.MediaStore
import android.view.WindowManager
import com.whoisthatgoodboi.Base.Config

fun Context.getSharedPrefs() = getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE)
internal val Context.windowManager: WindowManager get() = getSystemService(Context.WINDOW_SERVICE) as WindowManager

val Context.config: Config get() = Config.newInstance(applicationContext)

val Context.realScreenSize: Point
    get() {
        val size = Point()
        windowManager.defaultDisplay.getRealSize(size)
        return size
    }

fun Context.getLatestMediaId(uri: Uri = MediaStore.Files.getContentUri("external")): Long {
    val MAX_VALUE = "max_value"
    val projection = arrayOf("MAX(${BaseColumns._ID}) AS $MAX_VALUE")
    var cursor: Cursor? = null
    try {
        cursor = contentResolver.query(uri, projection, null, null, null)
        if (cursor?.moveToFirst() == true) {
            return cursor.getLongValue(MAX_VALUE)
        }
    } finally {
        cursor?.close()
    }
    return 0
}