package com.whoisthatgoodboi.Extentions


val RATIO_TOLERANCE = 0.1f

// permissions
const val PERMISSION_READ_STORAGE = 1
const val PERMISSION_WRITE_STORAGE = 2
const val PERMISSION_CAMERA = 3
const val PERMISSION_RECORD_AUDIO = 4
const val PERMISSION_READ_CONTACTS = 5
const val PERMISSION_WRITE_CONTACTS = 6
const val PERMISSION_READ_CALENDAR = 7
const val PERMISSION_WRITE_CALENDAR = 8
const val PERMISSION_CALL_PHONE = 9

// shared preferences
const val PREFS_KEY = "Prefs"

val ORIENT_PORTRAIT = 0
val ORIENT_LANDSCAPE_LEFT = 1
val ORIENT_LANDSCAPE_RIGHT = 2

// shared preferences
val SHOW_PREVIEW = "show_preview"
val FOCUS_BEFORE_CAPTURE = "focus_before_capture"
val TURN_FLASH_OFF_AT_STARTUP = "turn_flash_off_at_startup"
val LAST_USED_CAMERA = "last_used_camera"
val FLASHLIGHT_STATE = "flashlight_state"
val BACK_PHOTO_RESOLUTION_INDEX = "back_photo_resolution_index"
val PHOTO_PREVIEW_HINT_SHOWN = "photo_preview_hint_shown"
val KEEP_SETTINGS_VISIBLE = "keep_settings_visible"
val ALWAYS_OPEN_BACK_CAMERA = "always_open_back_camera"

val FLASH_OFF = 0
val FLASH_ON = 1
val FLASH_AUTO = 2

const val GRAPH_FILE_PATH = "file:///android_asset/simple.pb"
const val LABELS_FILE_PATH = "file:///android_asset/classes.txt"

const val GRAPH_INPUT_NAME = "conv2d_1_input"
const val GRAPH_OUTPUT_NAME = "dense_3/Softmax"

const val IMAGE_SIZE = 128
const val COLOR_CHANNELS = 3

const val ASSETS_PATH = "file:///android_asset/"