package com.whoisthatgoodboi.Extentions

import android.hardware.Camera

fun Camera.Size.isSixteenToNine(): Boolean {
    val selectedRatio = Math.abs(width / height.toFloat())
    val checkedRatio = 16 / 9.toFloat()
    val diff = Math.abs(selectedRatio - checkedRatio)
    return diff < RATIO_TOLERANCE
}