package com.whoisthatgoodboi.Classification

class ClassificationName(name: String, percent: Float) {
    var className : String = name
    var classPercent : Float = percent
}