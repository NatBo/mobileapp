package com.whoisthatgoodboi.Classification

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.whoisthatgoodboi.Base.BaseSimpleActivity
import com.whoisthatgoodboi.Classification.ImageClassifier.Classifier
import com.whoisthatgoodboi.Classification.ImageClassifier.ImageClassifierFactory
import com.whoisthatgoodboi.Classification.ImageClassifier.Result
import com.whoisthatgoodboi.Extentions.*
import com.whoisthatgoodboi.R
import com.whoisthatgoodboi.Utils.getCroppedBitmap
import kotlinx.android.synthetic.main.activity_classification.*
import java.util.*

class ClassificationActivity : BaseSimpleActivity() {

    private lateinit var mPhoto : String
    private lateinit var adapter: ClassificationAdapter
    private var classDataset : ArrayList<ClassificationName> = ArrayList()
    private lateinit var manager : RecyclerView.LayoutManager

    private lateinit var classifier : Classifier
    private val handler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        mPhoto = intent.getStringExtra("photo")
        Log.e("ClassificationActivity", mPhoto)
        initClassifier()
        classifyPhoto()
        initPhoto()
        initAdapterAndLaoutManaer()
        classes_recycler_view.layoutManager = manager
        classes_recycler_view.adapter = adapter

    }

    private fun classifyPhoto() {
        val uri = Uri.parse(mPhoto)
        val photoBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
        val croppedBitmap = getCroppedBitmap(photoBitmap)

        classifyAndGetResults(croppedBitmap)
    }

    private fun classifyAndGetResults(bitmap: Bitmap) {
        runInBackground(
                Runnable {
                    val results = classifier.recognizeImage(bitmap)
                    Log.e("Classify", results.toString())
                    showResults(results)
                }
        )
    }

    private fun showResults(queue: PriorityQueue<Result>) {
        val list = queue.sortedByDescending({it.confidence})
        for (r in list) {
            val percent = r.confidence * 100
            if (percent > 0.01) {
                classDataset.add(ClassificationName(r.result, percent))
            }
        }
    }

    private fun runInBackground(runnable: Runnable) {
        handler.post(runnable)
    }

    private fun initClassifier() {
        classifier = ImageClassifierFactory.create(
                assets,
                GRAPH_FILE_PATH,
                LABELS_FILE_PATH,
                IMAGE_SIZE,
                GRAPH_INPUT_NAME,
                GRAPH_OUTPUT_NAME
        )
    }

    private fun initAdapterAndLaoutManaer() {
        adapter = ClassificationAdapter(classDataset)
        manager = LinearLayoutManager(this)
    }

    private fun initPhoto() {
        val options = RequestOptions()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.NONE)

        Glide.with(this)
                .load(mPhoto)
                .apply(options)
                .into(photo)
    }
}