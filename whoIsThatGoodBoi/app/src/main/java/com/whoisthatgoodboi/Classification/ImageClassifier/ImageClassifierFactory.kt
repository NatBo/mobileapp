package com.whoisthatgoodboi.Classification.ImageClassifier

import android.content.res.AssetManager
import com.whoisthatgoodboi.Utils.getLabels
import org.tensorflow.contrib.android.TensorFlowInferenceInterface

object ImageClassifierFactory {
    fun create(
            assetManager: AssetManager,
            modelFilePath : String,
            labelsFilePath : String,
            imageSize : Int,
            inputName : String,
            outputName : String
    ) : Classifier {
        val labels = getLabels(assetManager, labelsFilePath)

        return ImageClassifier(
                inputName,
                outputName,
                imageSize.toLong(),
                labels,
                IntArray(imageSize * imageSize),
                FloatArray(imageSize * imageSize * 3),
                FloatArray(labels.size),
                TensorFlowInferenceInterface(assetManager, modelFilePath)
        )
    }
}