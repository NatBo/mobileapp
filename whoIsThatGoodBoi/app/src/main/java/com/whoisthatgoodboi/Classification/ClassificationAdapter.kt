package com.whoisthatgoodboi.Classification

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.whoisthatgoodboi.R
import kotlinx.android.synthetic.main.class_list_element_view.view.*
import java.text.DecimalFormat

class ClassificationAdapter(private val dataset: ArrayList<ClassificationName>) : RecyclerView.Adapter<ClassificationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val item = LayoutInflater.from(parent.context).inflate(R.layout.class_list_element_view, parent, false) as View
        return ViewHolder(item)
    }

    override fun getItemCount() = dataset.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.class_name.text = dataset[position].className
        holder.view.class_percent.text = DecimalFormat("#.##").format(dataset[position].classPercent)
        holder.view.class_progress_bar.progress = dataset[position].classPercent.toInt()
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}