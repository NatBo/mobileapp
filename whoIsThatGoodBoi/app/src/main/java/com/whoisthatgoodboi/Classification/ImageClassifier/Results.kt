package com.whoisthatgoodboi.Classification.ImageClassifier

data class Result(val result: String, val confidence: Float)