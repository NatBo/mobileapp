package com.whoisthatgoodboi.Classification.ImageClassifier

import android.graphics.Bitmap
import java.util.*

interface Classifier {
    fun recognizeImage(bitmap: Bitmap) : PriorityQueue<Result>
}